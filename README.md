# 🐬 Dolphin - dWallet Monitoring CLI

A dWallet command line interface monitoring tool for node operators.

# 📄 Description

Dolphin is a CLI for dWallet that provides high level features for validator monitoring. Under the hood it uses dWallet validator prometheus metric exporter to check the health of the node.
Features provided:

- Health status: display node health status on the shell
- Alerts: send health alerts and ping status messages
- Integration: send messages to Discord, Telegram, PagerDuty and/or Slack

# 📦 Installation

First install external dependencies, for that refer to official docs.

- Git: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

After, clone Dolphin repository and copy the corresponding binary (osx / amd / intel) to your preferred bin location.

```sh
cd ~
git clone https://gitlab.com/blockscope-net/dolphin-v2.git
cd dolphin
cp dolphin_amd /usr/local/bin/dolphin
```

Check that the `dolphin` binary is accessible from your shell.

```sh
dolphin --help
```

# ⚙️ Config

Next step is configuring Dolphin via its config yaml file. Copy the config file template to your home directory and edit it to change the custom values for your validator account.

```sh
cp .dolphin ~/.dolphin
vim ~/.dolphin
```

Config file values. dWallet section is mandatory to work. Alerts section is for sending messages, you can leave values empty to disable any of the alerts. Generic section if for customizing interval times.

```yaml
# dWallet
sui_validator_name: YOUR_VALIDATOR_NAME # your validator name (for some metrics like performance)

sui_node_url: http://127.0.0.1 # your node url to query metrics
sui_node_metrics_port: 9184 # your node metrics port to query metrics

# Alerts
discord_username: "DISCORD_USERNAME" # Discord name that will appear in messages sent
discord_url: "https://discord.com/api/webhooks/122...88/wd...is" # your Discord webhook url

telegram_bot_id: "3...2:AB...kL" # your Telegram bot id
telegram_chat_id: "2...3" # your Telegram chat id

pagerduty_integration_key: "cb...h3" # your PagerDuty integration key

slack_oauth_token: "xoxb-2...3-2...4-0df...Od" # your Slack app oauth token
slack_channel_id: "E...C" # your Slack app channel id

# Generic
scan_interval_in_secs: 5 # interval to wait between metrics endpoint requests
ping_interval_in_secs: 36000 # interval to wait between sending ping messages

http_request_retries: 5 # number of HTTP request retries
http_request_retry_interval_in_secs: 5 # number of HTTP retry interval
```

# ⌨️ Usage

Command help can be displayed using the `-h` or `--help` flag.

## Commands help

```sh
# display dolphin help for scan command
dolphin scan --help
```

## Scan

The scan command is a cronjob that queries the node metrics port (each `scan_interval_in_secs`) and displays the node status info on the shell (status, version, sync status and performance).

It will send messages to the configured communication channels when important events occur.

```sh
# execute scan command
dolphin scan
```

## Ping Messages

If you want the scan command to also send ping messages containing node status info (each `ping_interval_in_secs`), use the `--ping` or `-p` flag.

The command to execute:

```sh
# execute scan command and send ping messages
dolphin scan -p
```

# 📸 Screenshots

#### Shell UI

![Shell UI](image-2.png)

#### Discord Messages

![Discord Ping](image.png)

![Discord Alert](image-3.png)

# 🤔 Troubleshooting

- Why doesn't my shell show the Dolphin UI colors?
  Probably your shell is not in 256 color mode. You can set that in your .bash_profile (or correspoding shell config file)

In bash:

```
export TERM=xterm-256color
```

In zsh:

```
export TERM=screen-256color
```

If your using Tmux and the problem persist, check this thread to config your Tmux https://unix.stackexchange.com/questions/1045/getting-256-colors-to-work-in-tmux

# 📨 Support

Send any related questions to blockscope@protonmail.com

# 💻 Contributing

Feel free to propose any node operations you want to be included in Dolphin, glad to review Pull Request from contributors 🏄‍♂️🌊.

# 🛣️ Roadmap

TBD

# 🏗️ Project status

WIP

# ✍️ Authors

Blockscope.net :: https://blockscope.net :: {📦}

# ©️ License

Apache License 2.0

https://www.apache.org/licenses/
